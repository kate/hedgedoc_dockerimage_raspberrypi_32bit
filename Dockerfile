# Dockerfile is under GPLv3 by Linuxserver https://github.com/linuxserver/docker-hedgedoc/blob/main/LICENSE
# file has been modified by kate to suit 32-bit pi infrastructure

FROM alpine:latest
RUN apk add curl jq wget
# set version label
ARG BUILD_DATE
ARG VERSION
ARG HEDGEDOC_RELEASE
LABEL build_version="hedgedoc-raspi version:- ${VERSION} Build-date:- ${BUILD_DATE}"

# environment settings
ENV NODE_ENV production
ENV PUPPETEER_SKIP_DOWNLOAD true
ENV YARN_CACHE_FOLDER=/tmp/.yarn

RUN \
  echo "**** install build packages ****" && \
  apk add -U --no-cache \
    fontconfig \
    font-noto \
    netcat-openbsd \
    nodejs-current && \
  apk add -U --no-cache --virtual=build-dependencies \
    build-base \
    git \
    icu-libs \
    npm \
    openssl-dev \
    python3-dev \
    sqlite-dev \
    yarn && \
  echo "**** install hedgedoc ****" && \
  if [ -z ${HEDGEDOC_RELEASE+x} ]; then \
    HEDGEDOC_RELEASE=$(curl -sX GET "https://api.github.com/repos/hedgedoc/hedgedoc/releases/latest" \
    | jq -r '.tag_name'); \
  fi && \
  curl -o \
    /tmp/hedgedoc.tar.gz -L \
    "https://github.com/hedgedoc/hedgedoc/releases/download/${HEDGEDOC_RELEASE}/hedgedoc-${HEDGEDOC_RELEASE}.tar.gz" && \
  mkdir -p \
    /app/hedgedoc && \
  tar xf /tmp/hedgedoc.tar.gz -C \
    /app/hedgedoc --strip-components=1 && \
  cd /app/hedgedoc && \
  yarn install --immutable && \
  yarn run build && \
  yarn workspaces focus --production && \
  echo "**** cleanup ****" && \
  yarn cache clean && \
  apk del --purge \
    build-dependencies && \
  rm -rf \
    $HOME/.npm \
    $HOME/.yarn \
    /tmp/* 

# add local files
COPY root/ / 

# ports and volumes
EXPOSE 3000
VOLUME /config
